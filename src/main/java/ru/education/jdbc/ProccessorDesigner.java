package ru.education.jdbc;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProccessorDesigner {

    Integer id;
    String companyName;

    public ProccessorDesigner(Integer id, String companyName) {
        this.id = id;
        this.companyName = companyName;
    }
}
