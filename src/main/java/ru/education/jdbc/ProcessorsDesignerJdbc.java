package ru.education.jdbc;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ProcessorsDesignerJdbc {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public ProcessorsDesignerJdbc(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<ProccessorDesigner> findAll() {
        return jdbcTemplate.query("SELECT * FROM processors_designer", (resultSet, rowNum) ->
                new ProccessorDesigner(
                        resultSet.getInt("id"),
                        resultSet.getString("company_name")
                ));
    }

    public void create(String name) {
        jdbcTemplate.execute(String.format("insert into processors_designer (company_name) values (\'%s\')", name));
    }

    public void update(ProccessorDesigner proccessorDesigner) {
        jdbcTemplate.execute(String.format("update processors_designer set company_name = \'%s\' where id = %d",
                proccessorDesigner.companyName,
                proccessorDesigner.id));
    }
    public List<ProccessorDesigner> get(int id) {
        return jdbcTemplate.query(String.format("SELECT * FROM processors_designer WHERE id = %d", id), (resultSet, rowNum) ->
                new ProccessorDesigner(
                        resultSet.getInt("id"),
                        resultSet.getString("company_name")
                ));
    }
}
