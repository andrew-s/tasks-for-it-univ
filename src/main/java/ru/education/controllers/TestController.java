package ru.education.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;
import ru.education.entity.*;
import ru.education.jdbc.ProccessorDesigner;
import ru.education.entity.SalesPeriodJdbcRepository;
import ru.education.jdbc.ProcessorsDesignerJdbc;
import ru.education.jpa.*;
import ru.education.model.Formatter;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("api/v1")
public class  TestController {

    private final Formatter formatter;

    private final ProductRepository productRepository;

    private final SalesPeriodJdbcRepository salesPeriodJdbcRepository;

    private final SalesPeriodRepository salesPeriodRepository;

    private final ProcessorsRepository processorsRepository;

    private final ProcessorsDesignerJdbc processorsDesignerJdbc;

    @Autowired
    public TestController(@Qualifier("fooFormatter") Formatter formatter,
                          ProductRepository productRepository,
                          SalesPeriodJdbcRepository salesPeriodJdbcRepository,
                          SalesPeriodRepository salesPeriodRepository,
                          ProcessorsRepository processorsRepository,
                          ProcessorsDesignerJdbc processorsDesignerJdbc) {
        this.formatter = formatter;
        this.productRepository = productRepository;
        this.salesPeriodJdbcRepository = salesPeriodJdbcRepository;
        this.salesPeriodRepository = salesPeriodRepository;
        this.processorsRepository = processorsRepository;
        this.processorsDesignerJdbc = processorsDesignerJdbc;
    }

    /*------------Урок 2------------*/

    @GetMapping("/hello")
    public String getHello() {
        return "Hello, world!";
    }

    @GetMapping("/format")
    public String getFormat() {
        return formatter.format();
    }
    @GetMapping("/products")
    public List<Product> getProduct() {
        return productRepository.findAll();
    }

    @GetMapping("/sales/count")
    public Integer getSalesCount() {
        return salesPeriodJdbcRepository.count();
    }

    @GetMapping("/sales")
    public List<SalesPeriodJdbcDemo> getSalesPeriods() {
        return salesPeriodJdbcRepository.getSalesPeriod();
    }

    @GetMapping("/sales/byhigherprice")
    public List<SalesPeriodJdbcDemo> getSalesPeriodsByHigherPrice() {
        return salesPeriodJdbcRepository.getSalesPeriodPriceHigher(90);
    }

    @GetMapping("/products/sales/active")
    public List<Product> getProductsWhithActivePeriod() {
        return salesPeriodJdbcRepository.getProductsWithActivePeriod();
    }

    @PostMapping("/sales/jpa")
    public SalesPeriodJpaDemo addSalesPeriodsJpa(@RequestBody SalesPeriodJpaDemo salesPeriodJpaDemo) {
        return salesPeriodRepository.save(salesPeriodJpaDemo);
    }

    @GetMapping("/sales/jpa/max/price")
    public Integer getMaxPriceByProductId() {
        return salesPeriodRepository.getMaxPriceByProductId(1);
    }

    @GetMapping("/sales/jpa/active")
    public List<SalesPeriodJpaDemo> findByDateToIsNull() {
        return salesPeriodRepository.findByDateToIsNull();
    }

    @GetMapping("/sales/jpa/productname")
    public List<SalesPeriodJpaDemo> findByProductName() {
        return salesPeriodRepository.findByProductName("Car");
    }

    /*------------Таск 1------------*/

    @GetMapping("/jpa/hardware/procname")
    public List<String> getProcessorName() {
        return processorsRepository.getProcessorName(2);
    }

    @GetMapping("/jpa/hardware/existsprocmodel")
    public boolean existsByProcessorsModel() {
        return processorsRepository.existsByProcessorModel("3900x");
    }

    @GetMapping("/jpa/hardware/processorsinfo")
    public List<ProcessorsName> getProcessorsInfoByCompanyName() {
        return processorsRepository.getProcessorsInfoByCompanyName("Intel");
    }

    @GetMapping("/jdbc/hardware/getdesigners")
    public List<ProccessorDesigner> findAll() {
        return processorsDesignerJdbc.findAll();
    }

    @PostMapping("/jdbc/hardware/createdesigner")
    public void createDesigner() {
        processorsDesignerJdbc.create("Mediatek");
    }

    @PutMapping("/jdbc/hardware/updatedesigner")
    public void updateDesigner(@RequestBody ProccessorDesigner proccessorDesigner) {
        processorsDesignerJdbc.update(proccessorDesigner);
    }

    @GetMapping("/jdbc/hardware/getsingledesigner/{id}")
    public List<ProccessorDesigner> getSingleDesigner(@PathVariable(name = "id") int id) {
        return processorsDesignerJdbc.get(id);
    }
}
