package ru.education.entity;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import ru.education.entity.SalesPeriodJdbcDemo;
import ru.education.jpa.Product;

import java.util.List;

@Repository
public class SalesPeriodJdbcRepository {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public SalesPeriodJdbcRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public int count() {
        return jdbcTemplate.queryForObject("select count(*) from public.sales_period", Integer.class);
    }

    public List<SalesPeriodJdbcDemo> getSalesPeriod() {
        return jdbcTemplate.query("select * from public.sales_period", (resultSet, rowNum) ->
                new SalesPeriodJdbcDemo(
                      resultSet.getInt("id"),
                      resultSet.getInt("price"),
                      resultSet.getDate("date_from"),
                        resultSet.getDate("date_to"),
                        resultSet.getInt("product")
                ));
    }

    public List<SalesPeriodJdbcDemo> getSalesPeriodPriceHigher(int price) {
        return jdbcTemplate.query(String.format("select * from public.sales_period where price >= %d", price),
                (resultSet, rowNum) ->
                    new SalesPeriodJdbcDemo(
                            resultSet.getInt("id"),
                            resultSet.getInt("price"),
                            resultSet.getDate("date_from"),
                            resultSet.getDate("date_to"),
                            resultSet.getInt("product")
                    ));
    }

    public List<Product> getProductsWithActivePeriod() {
        return jdbcTemplate.query("select p.id product_id, p.name  product_name from public.product p inner join public.sales_period " +
                        "sp on p.id = sp.product where sp.date_to is null",
                (resultSet, rowNum) ->
                        new Product(
                                resultSet.getInt("product_id"),
                                resultSet.getString("product_name")
                        ));

    }
}
