package ru.education.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "processor_name")
public class ProcessorsName {

    @Id
    @Column(name = "id")
    Integer id;

    @Column(name = "processors_name")
    String processorName;

    @Column(name = "processors_model")
    String processorModel;

}
