package ru.education.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.education.jpa.Product;

import javax.persistence.*;
import java.util.Date;


/*@NoArgsConstructor чтобы преобразовать в json*/
@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "sales_period")
public class SalesPeriodJpaDemo {

    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sales_period_id_seq")
    @SequenceGenerator(name = "sales_period_id_seq", sequenceName = "sales_period_id_seq", allocationSize = 1)
    private long id;

    @Column(name = "price")
    private long price;

    @Column(name = "date_from")
    private Date dateFrom;

    @Column(name = "date_to")
    private Date dateTo;

    @OneToOne
    @JoinColumn(name = "product", referencedColumnName = "id", nullable = false)
    private Product product;
}
