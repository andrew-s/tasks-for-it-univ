package ru.education.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.education.entity.ProcessorsName;

import java.util.List;

@Repository
public interface ProcessorsRepository extends JpaRepository<ProcessorsName, Integer> {
    @Query(value = "select processors_name from processor_name where designer_id = :id", nativeQuery = true)
    List<String> getProcessorName(@Param("id") int companyId);

    boolean existsByProcessorModel(String processorsModel);

    @Query(value = "select * from processor_name where designer_id = (select id from processors_designer where company_name = :company)", nativeQuery = true)
    List<ProcessorsName> getProcessorsInfoByCompanyName(@Param("company") String company);
}
